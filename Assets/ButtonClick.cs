using UnityEngine;
using Unity.Netcode;
using UnityEngine.UI;
using TMPro;

public class ButtonClick : NetworkBehaviour
{
    [SerializeField] public Check check;
    [SerializeField] public Create create;
    [SerializeField] private Image image;
    [SerializeField] public GameObject imageC;
    private Scr scr;
    private Color color;
    [SerializeField] public TMP_Text text;
    public override void OnNetworkSpawn()
    {
        base.OnNetworkSpawn();
        image = this.gameObject.GetComponent<Image>();
        scr = this.gameObject.GetComponent<Scr>();
        if (!IsServer)
        {
            this.gameObject.GetComponent<Image>().color = scr.Color;
        }
        text = GameObject.FindGameObjectWithTag("Text").GetComponent<TMP_Text>();
        text.text = "P1";
        text.color = Color.red;
        color = this.gameObject.GetComponent<Image>().color;
    }

    [Rpc(SendTo.ClientsAndHost)]
    public void ClickRpc()
    {

        
        if (this.gameObject.GetComponent<Scr>().Color != Color.white)
        {
            return ;
        }
        else
        {
            if (check.GetPlayerID() == 0)
            {
                this.gameObject.GetComponent<Image>().color = Color.red;
                this.gameObject.GetComponent<Scr>().Color = Color.red;
                SeeChildIndex(this.gameObject);

            }
            else if (check.GetPlayerID() == 1)
            {
                this.gameObject.GetComponent<Image>().color = Color.blue;
                this.gameObject.GetComponent<Scr>().Color = Color.blue;
                SeeChildIndex(this.gameObject);
            }
        }
        if (text.color == Color.red)
        {
            text.text = "P2";
            text.color = Color.blue;
        }
        else if (text.color == Color.blue)
        {
            text.text = "P1";
            text.color = Color.red;
        }
        
            create.CheckforWinRpc();
            check.ChangePlayerServerRpc();
        
    }
    public void SeeChildIndex(GameObject child)
    {
        int x = child.transform.GetSiblingIndex();
        int y = child.transform.parent.GetSiblingIndex();
        Debug.Log("X = " + x + "Y = " + y);
    }
}