using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using Unity.Netcode;

public class Check : NetworkBehaviour
{
    public static int PlayerID { get; set; } = 0;
    public bool Host;
    public override void OnNetworkSpawn()
    {
        base.OnNetworkSpawn();
        if (IsHost)
        {
            Host = true;
        }
        else
        {
            Host= false;
        }
    }
    [Rpc(SendTo.ClientsAndHost)]
    public void ChangePlayerServerRpc()
    {
        if (PlayerID == 0) SetPlayerIdRpc(1);
        else if (PlayerID == 1) SetPlayerIdRpc(0);
        Debug.Log("PlayerID: " + PlayerID);
    }

    public void ChangePlayer()
    {
        ChangePlayerServerRpc();
        
    }

    public int GetPlayerID()
    {
        return PlayerID;
    }
    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            Debug.Log("is Client: " + IsClient);
            Debug.Log("is Host: " + IsHost);
            Debug.Log("Player ID : " + GetPlayerID());
        }
    }
    [Rpc(SendTo.ClientsAndHost)]
    public void SetPlayerIdRpc(int id)
    {
        PlayerID = id;
    }
}