using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using UnityEngine;

using Unity.Netcode;
using UnityEngine.UI;
using Unity.VisualScripting;

public class Create : NetworkBehaviour
{
    public int X;
    public int Y;
    public int NumberInRow;

    public GameObject prefVertical;
    public GameObject prefHorizontal;
    public GameObject prefImage;
    public GameObject Canvas;

    public GameObject Wins;

    public Scr scr;
    public List<GameObject> alllist;
    static Dictionary<Color, string> colorDictionary = new Dictionary<Color, string>();
    public override void OnNetworkSpawn()
    {
        base.OnNetworkSpawn();
        Wins = GameObject.FindGameObjectWithTag("Wins");
        Canvas = GameObject.FindGameObjectWithTag("Canvas");
        Wins = Canvas.transform.GetChild(1).gameObject;
    }

    private void Start()
    {
        if (!IsHost)
        {
            SortViaNetID();

            int id = 1;
            for (int i = 0; i < Y; i++)
            {
                for (int j = 0; j < X; j++)
                {
                    GameObject go = this.gameObject.transform.GetChild(i).GetChild(j).gameObject;
                    alllist.Add(go);
                    go.GetComponent<Scr>().Color = Color.white;
                    go.GetComponent<Image>().color = Color.white;
                    go.GetComponent<Scr>().ID = id;
                    id++;
                }
            }
            return;
        }

        Canvas = GameObject.FindGameObjectWithTag("Canvas");
        colorDictionary.Add(Color.white, "White");
        colorDictionary.Add(Color.red, "Red");
        colorDictionary.Add(Color.blue, "Blue");
        Wins = GameObject.FindGameObjectWithTag("Wins");
        Wins = Canvas.transform.GetChild(1).gameObject;
        CreateTableRpc(X, Y);
    }
    private void SortViaNetID()
    {
        List<NetworkObject> children = new List<NetworkObject>();

        for (int i = 0; i < this.transform.childCount; i++)
        {
            NetworkObject childNetworkObject = this.transform.GetChild(i).GetComponent<NetworkObject>();
            if (childNetworkObject != null)
            {
                children.Add(childNetworkObject);
            }
        }
        children.Sort((a, b) => a.NetworkObjectId.CompareTo(b.NetworkObjectId));
        for (int i = 0; i < children.Count; i++)
        {
            children[i].transform.SetSiblingIndex(i);
        }
        foreach (NetworkObject childNetworkObject in children)
        {
            List<NetworkObject> grandchildren = new List<NetworkObject>();

            for (int j = 0; j < childNetworkObject.transform.childCount; j++)
            {
                NetworkObject grandchildNetworkObject = childNetworkObject.transform.GetChild(j).GetComponent<NetworkObject>();
                if (grandchildNetworkObject != null)
                {
                    grandchildren.Add(grandchildNetworkObject);
                }
            }

            grandchildren.Sort((a, b) => a.NetworkObjectId.CompareTo(b.NetworkObjectId));
            for (int k = 0; k < grandchildren.Count; k++)
            {
                grandchildren[k].transform.SetSiblingIndex(k);
            }
        }
    }
    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Mouse0))
        {
            CheckforWinRpc();
        }
        
    }
    [Rpc(SendTo.ClientsAndHost)]
    public void CreateTableRpc(int x, int y)
    {
        int id = 1;
        for (int i = 0; i < Y; i++)
        {
            GameObject hor = Instantiate(prefHorizontal, prefVertical.transform);
            hor.GetComponent<NetworkObject>().Spawn();
            hor.transform.SetParent(prefVertical.transform);
            for (int j = 0; j < X; j++)
            {
                GameObject go = Instantiate(prefImage, hor.transform);
                alllist.Add(go);
                go.GetComponent<NetworkObject>().Spawn();
                go.transform.SetParent(hor.transform);
                go.GetComponent<Scr>().ID = id;
                go.GetComponent<Scr>().Color = Color.white;
                go.GetComponent<Scr>().Color.a = 1;
                go.GetComponent<Image>().color = go.GetComponent<Scr>().Color;
                go.GetComponent<ButtonClick>().create = this;
                go.GetComponent<ButtonClick>().check = Canvas.GetComponent<Check>();
                id++;
            }
        }
    }
    [Rpc(SendTo.ClientsAndHost)]
    public void RedWinsrpc()
    {
        Wins.SetActive(true);
        Wins.transform.GetChild(0).gameObject.SetActive(true);
        Wins.transform.GetChild(1).gameObject.SetActive(false);
    }
    [Rpc(SendTo.ClientsAndHost)]
    public void BlueWinsrpc()
    {
        Wins.SetActive(true);
        Wins.transform.GetChild(0).gameObject.SetActive(false);
        Wins.transform.GetChild(1).gameObject.SetActive(true);
    }
    [Rpc(SendTo.ClientsAndHost)]
    public void CheckforWinRpc()
    {
        
        CheckHor();
        CheckVer();
        CheckCross();
    }
    public void CheckHor()
    {
        int R = 0;
        int B = 0;
        int id = 0;
        for (int i = 0; i < Y; i++)
        {
            for (int j = 0; j < X; j++)
            {
                if (alllist[id].GetComponent<Scr>().Color == Color.blue) { B++; R = 0; }
                else if (alllist[id].GetComponent<Scr>().Color == Color.red) { R++; B = 0; }
                else { R = 0; B = 0; }
                if (R >= NumberInRow)
                {
                    //redwin
                    RedWinsrpc();
                    Debug.Log("Red Player wins!");
                }
                if (B >= NumberInRow)
                {
                    //bluewin
                    BlueWinsrpc();
                    Debug.Log("Blue Player wins!");
                }

                id++;
            }

            R = 0;
            B = 0;
        }
    }
    public void CheckVer()
    {
        int R = 0;
        int B = 0;
        int id = 0;
        for (int i = 1; i <= X; i++)
        {
            for (int j = 0; j < alllist.Count; j += X)
            {

                if (alllist[id].GetComponent<Scr>().Color == Color.blue) { B++; R = 0; }
                else if (alllist[id].GetComponent<Scr>().Color == Color.red) { R++; B = 0; }
                else { R = 0; B = 0; }
                //Debug.Log("id= " + id + ", X= " + i + ", Y= " + j + ", B= " + B + ", R= " + R);
                if (R >= NumberInRow)
                {
                    RedWinsrpc();
                    //redwin
                    Debug.Log("Red Player wins!");
                }
                if (B >= NumberInRow)
                {
                    //bluewin
                    BlueWinsrpc();
                    Debug.Log("Blue Player wins!");
                }
                id += X;
                if (id >= alllist.Count)
                {
                    id = 0;
                }
            }

            R = 0;
            B = 0;
            id += i;
        }
    }
    void CheckCross()
    {
        for (int i = 0; i < alllist.Count - (X * (NumberInRow - 1)); i++)
        {
            if (i % X <= (X - NumberInRow))
            {
                Checkright(Color.red, i);
                Checkright(Color.blue, i);

            }
            if (i % X >= (NumberInRow - 1))
            {
                Checkleft(Color.red, i);
                Checkleft(Color.blue, i);

            }

        }
    }
    void Checkright(Color color, int id)
    {
        int f = 0;

        for (int i = id; i < alllist.Count; i += (X + 1))
        {
            if (alllist[i].GetComponent<Scr>().Color == color)
            {
                f++;
            }
            else
            {
                return;
            }
            if (f >= NumberInRow)
            {
                //player color win
                if (color == Color.red)
                {
                    RedWinsrpc();
                }
                else if (color == Color.blue)
                {
                    BlueWinsrpc();
                }
                Debug.Log(colorDictionary[color] + " Player wins");
            }

        }

    }
    void Checkleft(Color color, int id)
    {
        int f = 0;

        for (int i = id; i < alllist.Count; i += (X - 1))
        {
            if (alllist[i].GetComponent<Scr>().Color == color)
            {
                f++;
            }
            else
            {
                return;
            }
            if (f >= NumberInRow)
            {
                //player color win
                if (color == Color.red)
                {
                    RedWinsrpc();
                }
                else if (color == Color.blue)
                {
                    BlueWinsrpc();
                }
                Debug.Log(colorDictionary[color] + " Player wins");
            }
        }


    }
}
