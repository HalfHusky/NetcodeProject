using System.Collections;
using System.Collections.Generic;
using Unity.Netcode;
using UnityEngine;

public class GameMenager : NetworkBehaviour
{
    [SerializeField] private int maxPlayers = 2;


    public override void OnNetworkSpawn()
    {
        base.OnNetworkSpawn();

        if (!IsHost)
        {
            gameObject.SetActive(false);
            return;
        }


        NetworkManager.Singleton.OnClientConnectedCallback += OnClientConnected;
    }

    private void OnClientConnected(ulong id)
    {
        if (NetworkManager.ConnectedClientsIds.Count < maxPlayers)
            return;

    }



}
