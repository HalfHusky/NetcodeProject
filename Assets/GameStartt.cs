using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Text.RegularExpressions;
using TMPro;
using Unity.Netcode;
using Unity.Netcode.Transports.UNET;
using Unity.Netcode.Transports.UTP;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameStartt : NetworkBehaviour
{
    [SerializeField] private Button hostbutton;
    [SerializeField] private Button joinbutton;
    [SerializeField] private Button exitbutton;
    [SerializeField] NetworkManager networkMenager;
    [SerializeField] private TMP_InputField ipAddressInputField;
    [SerializeField] private TMP_InputField portInputField;
    void Start()
    {
        if (!IsOwner)
        {
            return;
        }
        joinbutton.onClick.AddListener(JoinGame);
        hostbutton.onClick.AddListener(HostGame);
        exitbutton.onClick.AddListener(QuitGame);
    }
    public void JoinGame()
    {
        
        var conectInfo = ipAddressInputField.text.Split(":");
        ((UnityTransport)NetworkManager.NetworkConfig.NetworkTransport).SetConnectionData(conectInfo[0], ushort.Parse(conectInfo[1]), conectInfo[0]);
        NetworkManager.StartClient();
    }
    public void HostGame()
    {
        var conectInfo = ipAddressInputField.text.Split(":");
        if (ushort.TryParse(conectInfo[1], out ushort port))
        {
            // Parsing was successful, use the port value
            Console.WriteLine("Port: " + port);
            ((UnityTransport)NetworkManager.NetworkConfig.NetworkTransport).SetConnectionData(conectInfo[0], ushort.Parse(conectInfo[1]), conectInfo[0]);
        }
        else
        {
            // Parsing failed, handle the error
            Console.WriteLine("Invalid port number: " + conectInfo[1]);
            return;
        }
        NetworkManager.StartHost();
        NetworkManager.SceneManager.LoadScene("SampleScene", LoadSceneMode.Single);
        Debug.Log("AAAAA");
    }
    public void QuitGame()
    {
        Application.Quit();
    }
}
