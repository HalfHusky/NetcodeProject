using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Netcode;

public class InteractiveBlock : NetworkBehaviour
{

    public Check check;
    public GameObject Shroud;
    public override void OnNetworkSpawn()
    {
        base.OnNetworkSpawn();
        check = GameObject.FindGameObjectWithTag("Canvas").GetComponent<Check>();
        Shroud = GameObject.FindGameObjectWithTag("Shroud");
        
    }
    [ClientRpc]
    private void RpcShroudCLogicClientRpc()
    {
        int z = check.GetPlayerID();
        if (z == 0)
        {
            Shroud.SetActive(true);
        }
        else
        {
            Shroud.SetActive(false);
        }
    }
    [ServerRpc]
    private void RpcShroudSLogicServerRpc()
    {
        int z = check.GetPlayerID();
        if (z == 1)
        {
            Shroud.SetActive(true);
        }
        else
        {
            Shroud.SetActive(false);
        }
    }
    private void Update()
    {
        RpcShroudCLogicClientRpc();
        if (IsHost)
        {

        RpcShroudSLogicServerRpc();
        }
    }
}
