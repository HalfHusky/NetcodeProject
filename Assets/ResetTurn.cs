using System.Collections;
using System.Collections.Generic;
using Unity.Netcode;
using UnityEngine;
using UnityEngine.SceneManagement;
using System.Linq;
public class ResetTurn : MonoBehaviour
{
    private void Start()
    {
        if (this.transform.GetChild(1).gameObject.activeSelf)
        {
            this.transform.GetChild(1).gameObject.SetActive(false);
            this.transform.GetChild(1).GetChild(0).gameObject.SetActive(false);
            this.transform.GetChild(1).GetChild(1).gameObject.SetActive(false);
        }
        
    }
    [Rpc(SendTo.ClientsAndHost)]
    public void ResetGameRpc()
    {

        this.transform.GetChild(3).gameObject.SetActive(false);

        SceneManager.LoadScene(0); // Load scene 0
        NetworkManager.Singleton.Shutdown(); // Disconnect from the server
        Application.Quit();
        
    }

}
