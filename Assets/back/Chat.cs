using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Netcode;
using TMPro;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UnityEngine.UIElements;

public class Chat : NetworkBehaviour
{
    [SerializeField] private TMP_InputField messageField;
    [SerializeField] private UnityEngine.UI.Button sendButton;
    [SerializeField] private Scrollbar scrollBar;

    [SerializeField] private TextMeshProUGUI messagePref;
    [SerializeField] private RectTransform messageParentd;

    public override void OnNetworkSpawn()
    {
        base.OnNetworkSpawn();
        sendButton.onClick.AddListener(TrySendMessage);
    }
    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Return)) 
        { 
            TrySendMessage();
        }
    }
    private void TrySendMessage()
    {
        if (messageField.text.Length == 0) return;
        string prefix = IsHost ? "{Host}" : "{Client}";
        string nickname = NetworkManager.Singleton.LocalClient.PlayerObject.GetComponent<PlayerScript>().Nickname;
        string msg = $"{prefix} {nickname}: {messageField.text}";
        SendMessageRpc(msg);
        messageField.text = string.Empty;
        EventSystem.current.SetSelectedGameObject(messageField.gameObject, null);
        messageField.OnPointerClick(new PointerEventData(EventSystem.current) { button = PointerEventData.InputButton.Left});
        scrollBar.value = 0;
    }
    [Rpc(SendTo.ClientsAndHost)]
    private void SendMessageRpc(string message)
    {
        TextMeshProUGUI label = Instantiate(messagePref, messageParentd);
        label.text = message;
        
    }
}
