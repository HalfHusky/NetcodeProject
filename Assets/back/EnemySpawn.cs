using System.Collections;
using System.Collections.Generic;
using Unity.Netcode;
using UnityEngine;

public class EnemySpawn : NetworkBehaviour
{
    [SerializeField] private NetworkObject enemyPrefab;
    [SerializeField] private float spawnRate = 2f;
    [SerializeField] private float enemySpeed = 2f;
    [SerializeField] private Transform enemiesParent;

    private float _counter = 0f;
    public bool IsInitialized = false;
    public override void OnNetworkSpawn()
    {
        base.OnNetworkSpawn();

        if (!IsHost)
        {
            gameObject.SetActive(false);
            return;
        }
    }


    private void Update()
    {
        if (!IsInitialized)
        {
            return;
        }
        if (_counter < spawnRate)
        {
            _counter += Time.deltaTime;

        }
        else
        {
            _counter = 0;
            var randomChildIndex = (Random.Range(0, transform.childCount));
            Transform child = transform.GetChild(randomChildIndex);

            NetworkObject spawned = NetworkManager.SpawnManager.InstantiateAndSpawn(enemyPrefab);
            spawned.transform.SetParent(enemiesParent);
            spawned.transform.position = child.position;
            spawned.transform.rotation = Quaternion.Euler(-90f, 0f, 0f);
            Rigidbody rbEnemy = spawned.GetComponent<Rigidbody>();
            
            rbEnemy.isKinematic = false;
            rbEnemy.velocity = (randomChildIndex == 0 ? Vector3.right : Vector3.left) * enemySpeed;
            Destroy(rbEnemy.gameObject, 5f);
        }
    }

    public void Reset()
    {
        _counter = 0;
        foreach (Transform child in enemiesParent) Destroy(child.gameObject);

    }
}
