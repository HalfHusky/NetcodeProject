using System.Collections;
using System.Collections.Generic;
using Unity.Netcode;
using UnityEngine;

public class GameManager : NetworkBehaviour
{
    [SerializeField] private int maxPlayers = 3;

    [SerializeField] private PlayerSpawner playerSpanwer;
    [SerializeField] private EnemySpawn enemySpawn;

    public override void OnNetworkSpawn()
    {
        base.OnNetworkSpawn();

        if(!IsHost)
        {
            gameObject.SetActive(false);
            return;
        }

        
        NetworkManager.Singleton.OnClientConnectedCallback += OnClientConnected;
    }

    private void OnClientConnected(ulong id)
    {
        playerSpanwer.Spawner(id);
        if (NetworkManager.ConnectedClientsIds.Count < maxPlayers)
            return;

        enemySpawn.IsInitialized = true;
    }

    private int _playerLost = 0;
    [Rpc(SendTo.Server)]
    public void PlayerLostRpc()
    {
        _playerLost++;
        if(_playerLost >= maxPlayers)
        {
            Debug.Log("Reset game");

            foreach(ulong id in NetworkManager.ConnectedClientsIds)
            {
                playerSpanwer.Spawner(id);
            }
            enemySpawn.Reset();
            _playerLost = 0;

        }
    }


}
