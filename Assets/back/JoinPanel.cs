using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Unity.Netcode;
using TMPro;
using UnityEngine.SceneManagement;
using Unity.Netcode.Transports.UTP;

public class JoinPanel : NetworkBehaviour
{
    [SerializeField] private Button hostbutton;
    [SerializeField] private Button joinbutton;
    [SerializeField] private TMP_InputField inputField;


    void Start()
    {
        if (!IsOwner)
        {
            return;
        }
        joinbutton.onClick.AddListener(JoinGame);
        hostbutton.onClick.AddListener(HostGame);
    }
    public void JoinGame()
    {
        var conectInfo = inputField.text.Split(":");
        ((UnityTransport)NetworkManager.NetworkConfig.NetworkTransport).SetConnectionData(conectInfo[0], ushort.Parse(conectInfo[1]), conectInfo[0]);
        NetworkManager.StartClient();
        
    }
    public void HostGame()
    {
        NetworkManager.StartHost();
        
    }
    public override void OnNetworkSpawn()
    {
        base.OnNetworkSpawn();
        if (!IsHost) return;
        NetworkManager.Singleton.OnServerStarted += () =>
        {
            NetworkManager.SceneManager.LoadScene("Lobby", LoadSceneMode.Single);
            Debug.Log("AAAAAAAAAAAAAA");
        };

    }

}
