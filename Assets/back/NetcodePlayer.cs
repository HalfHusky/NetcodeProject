using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Netcode;


[RequireComponent(typeof(Rigidbody))]

public class NetcodePlayer : NetworkBehaviour
{
    [SerializeField] private float junpForce = 10f;
    [SerializeField] private string message;

    public override void OnNetworkSpawn()
    {
        base.OnNetworkSpawn();
        if (!IsOwner)
            return;

        NetworkManager.Singleton.OnClientConnectedCallback += OnClientConnected;

    }

    private void OnClientConnected(ulong id)
    {
        Debug.Log($"Client connected with ID: {id}");
    }


    private void Update()
    {
        if (!IsOwner)
        {
            return;
        }
        if (Input.GetKeyDown(KeyCode.Return))
        {
            SendMessageRpc(message);
        }
        if (Input.GetKeyUp(KeyCode.Space))
            JumpRpc(NetworkManager.LocalClientId);
    }

    [Rpc(SendTo.Server)]
    public void JumpRpc(ulong clientID)
    {
    //    if (Input.GetKeyUp(KeyCode.Space))
        {
            NetworkManager.ConnectedClients[clientID].PlayerObject.GetComponent<Rigidbody>().velocity = Vector3.up * junpForce;
            
        }
    }

    [Rpc(SendTo.ClientsAndHost)]
    private void SendMessageRpc(string msg)
    {
        Debug.Log(msg);
    }


    private void OnTriggerEnter(Collider other)
    {
        if(!IsHost) { return; }
        transform.position = new Vector3(0, -30, 0);
        FindObjectOfType<GameManager>().PlayerLostRpc();
    }


}
