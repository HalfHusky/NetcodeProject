using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Netcode;
using TMPro;
using UnityEngine.UI;

public class NicknamePanel : NetworkBehaviour
{
    [SerializeField] private TMP_InputField nicknameInput;
    [SerializeField] private Button acceptbutton;


    private void Start()
    {
        nicknameInput.text = string.Empty;
        acceptbutton.onClick.AddListener(OnNicknameAccept);

       
    }
    public void OnNicknameAccept()
    {
        NetworkManager.Singleton.LocalClient.PlayerObject.GetComponent<PlayerScript>().Nickname = nicknameInput.text;
    }
}
