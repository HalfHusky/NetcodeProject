using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Netcode;

public class PlayerSpawner : NetworkBehaviour
{

    private float _xOffset = 0;
    public override void OnNetworkSpawn()
    {
        base.OnNetworkSpawn();

        if(!IsHost)
        {
            gameObject.SetActive(true); return;
        }
    }


    public void Spawner(ulong id)
    {
        _xOffset = id * 2f;
        NetworkObject player = NetworkManager.Singleton.ConnectedClients[id].PlayerObject;
        player.gameObject.transform.position = new Vector3(_xOffset, 0, 0);
    }
}
